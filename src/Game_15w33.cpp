//============================================================================
// Name        : Game_15w33.cpp
// Author      : Daniel Fernández Villanueva
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "game/GameEngine.h"
#include "contrib/rapidxml/rapidxml.hpp"
#include "contrib/rapidxml/rapidxml_print.hpp"

using namespace std;
using namespace rapidxml;

int main()
{
	game::GameEngine game;
	game.run();
	/*xml_document<> doc;
	xml_node<> *node = doc.allocate_node(node_element, "config");
	doc.append_node(node);
	node->append_node(doc.allocate_node(node_element, "window"));
	node->first_node("window")->append_node(doc.allocate_node(node_element, "size"));
	node->first_node("window")->first_node("size")->append_node(doc.allocate_node(node_element, "x", "100.0"));
	node->first_node("window")->first_node("size")->append_node(doc.allocate_node(node_element, "y", "200.0"));

	ofstream os;
	os.open("ser.txt");
	os << doc;
	os.close();*/


	//xml_attribute<> *attr = doc.allocate_attribute("href", "google.com");
	//node->append_attribute(attr);
	//cout << doc;

	return 0;
}
