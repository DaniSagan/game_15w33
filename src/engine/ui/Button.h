/*
 * Button.h
 *
 *  Created on: Aug 15, 2015
 *      Author: daniel
 */

#ifndef ENGINE_UI_BUTTON_H_
#define ENGINE_UI_BUTTON_H_

#include "DrawableControl.h"
#include "../../resources/Fonts.h"
#include "../../tools/geometry/Geometry.h"
#include "../../tools/serialization/Printer.h"
#include <iostream>

namespace game
{

class Button: public DrawableControl
{
public:
	Button(DrawableControl* lpParent);
	virtual ~Button();

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
	virtual void handleEvent(sf::Event& event);
	virtual void onLeftClick();
	virtual void onMiddleClick();
	virtual void onRightClick();
	virtual void onMouseOver();
	virtual void onMouseOut();

	const sf::Text& getText() const;
	void setText(const sf::Text& text);
	void setString(const string& str);

	virtual void setPosition(const sf::Vector2f& position);
	virtual void setSize(const sf::Vector2f& size);

	enum State
	{
		NORMAL,
		OVER
	};

	enum ActionID
	{
		MOUSE_OVER,
		MOUSE_OUT,
		MOUSE_LEFT_CLICK,
		MOUSE_MIDDLE_CLICK,
		MOUSE_RIGHT_CLICK
	};

	struct
	{
		sf::Color normal;
		sf::Color over;
	} color;

protected:
	sf::Text text;
	sf::RectangleShape shape;
	State state;

	void updateTextPosition();
	void updateRect();
};

} /* namespace game */

#endif /* ENGINE_UI_BUTTON_H_ */
