/*
 * Button.cpp
 *
 *  Created on: Aug 15, 2015
 *      Author: daniel
 */

#include "Button.h"

namespace game
{

Button::Button(DrawableControl* lpParent):
		DrawableControl(lpParent), state(State::NORMAL)
{
	rect = sf::Rect<float>(sf::Vector2f(0.f, 0.f), sf::Vector2f(100.f, 25.f));
	color.normal = sf::Color(192, 192, 192);
	color.over = sf::Color(220, 220, 220);
	shape.setPosition(sf::Vector2f(0.f, 0.f));
	shape.setSize(sf::Vector2f(100.f, 25.f));
	shape.setFillColor(color.normal);
	text.setFont(Fonts::getInstance().font);
	text.setCharacterSize(12.f);
	text.setColor(sf::Color(0.f ,0.f ,0.f));
	text.setString("Button");
	updateTextPosition();
}

Button::~Button()
{
	// TODO Auto-generated destructor stub
}

void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(shape);
	target.draw(text);
	DrawableControl::draw(target, states);
}

void Button::onLeftClick()
{
	this->sendAction(Action(*this, ActionID::MOUSE_LEFT_CLICK));
}

void Button::onMiddleClick()
{
	this->sendAction(Action(*this, ActionID::MOUSE_MIDDLE_CLICK));
}

void Button::onRightClick()
{
	this->sendAction(Action(*this, ActionID::MOUSE_RIGHT_CLICK));
}

void Button::onMouseOver()
{
	shape.setFillColor(color.over);
	this->sendAction(Action(*this, ActionID::MOUSE_OVER));
}

void Button::onMouseOut()
{
	shape.setFillColor(color.normal);
	this->sendAction(Action(*this, ActionID::MOUSE_OUT));
}

const sf::Text& Button::getText() const
{
	return text;
}

void Button::setString(const string& str)
{
	text.setString(str);
	updateTextPosition();
}

void Button::setText(const sf::Text& text)
{
	this->text = text;
	updateTextPosition();
}

void Button::setPosition(const sf::Vector2f& position)
{
	DrawableControl::setPosition(position);
	shape.setPosition(position);
	updateRect();
	updateTextPosition();
}

void Button::setSize(const sf::Vector2f& size)
{
	DrawableControl::setSize(size);
	shape.setSize(size);
}

void Button::handleEvent(sf::Event& event)
{
	if(event.type == sf::Event::MouseMoved)
	{
		if(rect.contains(event.mouseMove.x, event.mouseMove.y) && state != State::OVER)
		{
			state = State::OVER;
			onMouseOver();
		}
		else if(!rect.contains(event.mouseMove.x, event.mouseMove.y) && state != State::NORMAL)
		{
			state = State::NORMAL;
			onMouseOut();
		}
	}
	else if(event.type == sf::Event::MouseButtonPressed)
	{
		if(state == State::OVER)
		{
			if(event.mouseButton.button == sf::Mouse::Left)
			{
				onLeftClick();
			}
			else if(event.mouseButton.button == sf::Mouse::Middle)
			{
				onMiddleClick();
			}
			else if(event.mouseButton.button == sf::Mouse::Right)
			{
				onRightClick();
			}
		}
	}
}

void Button::updateTextPosition()
{
	sf::Vector2f textCenter = Geometry::getCenter(text.getLocalBounds());
	sf::Vector2f buttonCenter = Geometry::getCenter(rect);
	text.setOrigin(Geometry::round(textCenter));
	text.setPosition(Geometry::round(buttonCenter));
}

void Button::updateRect()
{
	rect = sf::Rect<float>(shape.getPosition(), shape.getSize());
}

} /* namespace game */
