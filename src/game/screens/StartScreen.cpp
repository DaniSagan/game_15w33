/*
 * StartScreen.cpp
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#include "../../game/screens/StartScreen.h"

namespace game
{

StartScreen::StartScreen(sf::RenderWindow& window):
		Screen(window)
{
	// TODO Auto-generated constructor stub

}

StartScreen::~StartScreen()
{
	// TODO Auto-generated destructor stub
}

void StartScreen::tick()
{
	this->update();
	this->handleInput();
	this->draw();
}

void StartScreen::update()
{

}

void StartScreen::handleInput()
{
	sf::Event event;
	while(this->window.pollEvent(event))
	{
		switch(event.type)
		{
		case sf::Event::KeyPressed:
			switch(event.key.code)
			{
			case sf::Keyboard::Escape:
				this->sendAction(Action(*this, StartScreen::ActionID::GOTO_LOADINGSCREEN));
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}

}

void StartScreen::draw()
{
	this->window.clear(sf::Color(255, 0, 0));
	this->window.display();
}

void StartScreen::quit()
{
}

void StartScreen::actionPerformed(const Action& action)
{

}


} /* namespace game */
