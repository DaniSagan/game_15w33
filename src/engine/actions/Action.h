/*
 * Action.h
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#ifndef ENGINE_ACTIONS_ACTION_H_
#define ENGINE_ACTIONS_ACTION_H_

namespace game
{

class ActionSender;

class Action
{
public:
	Action(ActionSender& sender, int id);
	virtual ~Action();

	const ActionSender& getSender() const;
	int getId() const;

protected:
	ActionSender& sender;
	int id;
};

} /* namespace game */

#endif /* ENGINE_ACTIONS_ACTION_H_ */
