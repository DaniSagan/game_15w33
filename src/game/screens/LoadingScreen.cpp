/*
 * LoadingScreen.cpp
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#include "../../game/screens/LoadingScreen.h"

#include "../../engine/actions/Action.h"

namespace game
{

LoadingScreen::LoadingScreen(sf::RenderWindow& window):
		Screen(window), testButton(nullptr)
{
	testButton.addActionListener(this);
	testButton.setString("Quit program");

}

LoadingScreen::~LoadingScreen()
{
	// TODO Auto-generated destructor stub
}

void LoadingScreen::tick()
{
	update();
	handleInput();
	draw();
}

void LoadingScreen::update()
{

}

void LoadingScreen::handleInput()
{
	sf::Event event;
	while(this->window.pollEvent(event))
	{
		testButton.handleEvent(event);
		switch(event.type)
		{
		case sf::Event::Closed:
			sendAction(Action(*this, LoadingScreen::ActionID::QUIT));
			break;
		case sf::Event::KeyPressed:
			switch(event.key.code)
			{
			case sf::Keyboard::Escape:
				sendAction(Action(*this, LoadingScreen::ActionID::QUIT));
				break;
			case sf::Keyboard::S:
				sendAction(Action(*this, LoadingScreen::ActionID::GOTO_STARTSCREEN));
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}
}

void LoadingScreen::draw()
{
	window.clear(sf::Color(0, 0, 255));
	window.draw(testButton);
	window.display();
}

void LoadingScreen::quit()
{
}

void LoadingScreen::actionPerformed(const Action& action)
{
	if(action.getSender().getId() == testButton.getId())
	{
		if(action.getId() == Button::MOUSE_LEFT_CLICK)
		{
			cout << "Mouse Left Click!!!" << endl;
			sendAction(Action(*this, ActionID::QUIT));
		}
		else if(action.getId() == Button::MOUSE_MIDDLE_CLICK)
		{
			cout << "Mouse Middle Click!!!" << endl;
		}
		else if(action.getId() == Button::MOUSE_RIGHT_CLICK)
		{
			cout << "Mouse Right Click!!!" << endl;
		}
	}
}

} /* namespace game */
