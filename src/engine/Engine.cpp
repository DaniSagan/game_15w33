/*
 * Engine.cpp
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#include "Engine.h"

namespace game
{

Engine::Engine():
		lpCurrentScreen(nullptr)
{
}

Engine::~Engine()
{
}

void Engine::run()
{
	while(this->lpCurrentScreen != nullptr)
	{
		this->lpCurrentScreen->tick();
	}
}

void Engine::changeScreen(Screen* lpScreen)
{
	this->lpCurrentScreen = lpScreen;
}

void Engine::quit()
{
	this->lpCurrentScreen = nullptr;
}

void Engine::actionPerformed(const Action& action)
{
}

} /* namespace game */
