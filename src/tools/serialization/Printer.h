/*
 * Printer.h
 *
 *  Created on: Aug 16, 2015
 *      Author: daniel
 */

#ifndef TOOLS_SERIALIZATION_PRINTER_H_
#define TOOLS_SERIALIZATION_PRINTER_H_

#include <string>
#include <sstream>
#include <SFML/Graphics.hpp>

using namespace std;

namespace game
{

class Printer
{
public:
	Printer();
	virtual ~Printer();

	template <typename T>
	static string str(const sf::Vector2<T>& v)
	{
		stringstream ss;
		ss << v.x << ", " << v.y;
		return ss.str();
	}
};

} /* namespace game */

#endif /* TOOLS_SERIALIZATION_PRINTER_H_ */
