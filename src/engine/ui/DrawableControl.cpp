/*
 * DrawableControl.cpp
 *
 *  Created on: Aug 15, 2015
 *      Author: daniel
 */

#include "DrawableControl.h"

namespace game
{

DrawableControl::DrawableControl(DrawableControl* lpParent):
		Control(), lpParent(lpParent)
{
	// TODO Auto-generated constructor stub

}

DrawableControl::~DrawableControl()
{
	// TODO Auto-generated destructor stub
}

sf::Vector2f DrawableControl::getAbsolutePosition() const
{
	if(this->lpParent == nullptr)
	{
		return getPosition();
	}
	else
	{
		return this->lpParent->getAbsolutePosition() + getPosition();
	}
}

void DrawableControl::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	for(DrawableControl* lpChild: this->lpChildren)
	{
		lpChild->draw(target, states);
	}
}

const sf::Rect<float>& DrawableControl::getRect() const
{
	return rect;
}

void DrawableControl::setRect(const sf::Rect<float>& rect)
{
	this->rect = rect;
}

sf::Vector2f DrawableControl::getPosition() const
{
	return sf::Vector2f(rect.left, rect.top);
}

void DrawableControl::setPosition(const sf::Vector2f& position)
{
	rect.left = position.x;
	rect.top = position.y;
}

sf::Vector2f DrawableControl::getSize() const
{
	return sf::Vector2f(rect.width, rect.height);
}

void DrawableControl::setSize(const sf::Vector2f& size)
{
	rect.width = size.x;
	rect.height = size.y;
}

} /* namespace game */
