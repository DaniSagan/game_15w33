/*
 * Screen.h
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#ifndef SCREENS_SCREEN_H_
#define SCREENS_SCREEN_H_

#include "../actions/ActionSender.h"
#include "../actions/ActionListener.h"
#include <SFML/Graphics.hpp>

namespace game
{

class Screen: public ActionSender, public ActionListener
{
public:
	Screen(sf::RenderWindow& window);
	virtual ~Screen();

	virtual void tick() = 0;
	virtual void quit() = 0;

protected:
	sf::RenderWindow& window;
	bool running;
};

} /* namespace game */

#endif /* SCREENS_SCREEN_H_ */
