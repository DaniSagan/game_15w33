/*
 * Screen.cpp
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#include "Screen.h"

namespace game
{

Screen::Screen(sf::RenderWindow& window):
		ActionSender(), window(window), running(false)
{
}

Screen::~Screen()
{

}

} /* namespace game */
