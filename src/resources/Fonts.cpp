/*
 * Fonts.cpp
 *
 *  Created on: Aug 15, 2015
 *      Author: daniel
 */

#include "Fonts.h"

namespace game
{

Fonts& game::Fonts::getInstance()
{
	static Fonts instance;
	if(!instance.loaded)
	{
		instance.font.loadFromFile("resources/fonts/Open_Sans/OpenSans-Regular.ttf");
		cout << "Font loaded." << endl;
	}
	return instance;
}

}
