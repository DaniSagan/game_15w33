/*
 * Geometry.h
 *
 *  Created on: Aug 16, 2015
 *      Author: daniel
 */

#ifndef TOOLS_GEOMETRY_GEOMETRY_H_
#define TOOLS_GEOMETRY_GEOMETRY_H_

#include <SFML/Graphics.hpp>
#include <cmath>

namespace game
{

class Geometry
{
public:
	Geometry();
	virtual ~Geometry();

	template <typename T>
	static sf::Vector2<T> getCenter(const sf::Rect<T>& rect)
	{
		return sf::Vector2<T>(rect.left + rect.width/2, rect.top + rect.height/2);
	}

	template <typename T>
	static sf::Vector2<T> round(const sf::Vector2<T>& v)
	{
		return sf::Vector2<T>(floor(v.x), floor(v.y));
	}
};

} /* namespace game */

#endif /* TOOLS_GEOMETRY_GEOMETRY_H_ */
