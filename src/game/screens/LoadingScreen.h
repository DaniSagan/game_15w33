/*
 * LoadingScreen.h
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#ifndef GAME_SCREENS_LOADINGSCREEN_H_
#define GAME_SCREENS_LOADINGSCREEN_H_

#include "../../engine/screens/Screen.h"
#include <iostream>
#include "../../engine/ui/Button.h"

namespace game
{

class LoadingScreen: public Screen
{
public:
	LoadingScreen(sf::RenderWindow& window);
	virtual ~LoadingScreen();

	virtual void tick();
	virtual void update();
	virtual void handleInput();
	virtual void draw();
	virtual void quit();

	virtual void actionPerformed(const Action& action);

	enum ActionID
	{
		QUIT = 0,
		GOTO_STARTSCREEN,
	};
protected:
	Button testButton;
};

} /* namespace game */

#endif /* GAME_SCREENS_LOADINGSCREEN_H_ */
