/*
 * ActionSender.cpp
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#include "ActionSender.h"
#include "ActionListener.h"

namespace game
{

int ActionSender::count = 0;

ActionSender::ActionSender()
{
	this->id = ActionSender::count;
	++ActionSender::count;

}

ActionSender::~ActionSender()
{
	// TODO Auto-generated destructor stub
}

void ActionSender::addActionListener(ActionListener* actionListener)
{
	this->actionListeners.push_back(actionListener);
}

void ActionSender::sendAction(const Action& action)
{
	for(ActionListener* lpListener: this->actionListeners)
	{
		lpListener->actionPerformed(action);
	}
}

int ActionSender::getId() const
{
	return this->id;
}

} /* namespace game */
