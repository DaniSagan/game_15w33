/*
 * Engine.h
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#ifndef ENGINE_H_
#define ENGINE_H_

#include "../game/screens/LoadingScreen.h"
#include "../game/screens/StartScreen.h"
#include "screens/Screen.h"

using namespace std;

namespace game
{

class Engine: public ActionListener
{
public:
	Engine();
	virtual ~Engine();

	virtual void run();
	virtual void changeScreen(Screen* screen);
	virtual void quit();

	virtual void actionPerformed(const Action& action);

protected:
	Screen* lpCurrentScreen;

	//Screen* lpLoadingScreen;
	//Screen* lpStartScreen;

	sf::RenderWindow window;
};

} /* namespace game */

#endif /* ENGINE_H_ */
