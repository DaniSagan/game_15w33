/*
 * ActionSender.h
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#ifndef ENGINE_ACTIONS_ACTIONSENDER_H_
#define ENGINE_ACTIONS_ACTIONSENDER_H_

using namespace std;

#include <vector>
#include "Action.h"

namespace game
{

class ActionListener;

class ActionSender
{
public:
	ActionSender();
	virtual ~ActionSender();

	virtual void addActionListener(ActionListener* actionListener);
	virtual void sendAction(const Action& action);

	int getId() const;
protected:
	vector<ActionListener*> actionListeners;
	static int count;
	int id;
};

} /* namespace game */

#endif /* ENGINE_ACTIONS_ACTIONSENDER_H_ */
