/*
 * Fonts.h
 *
 *  Created on: Aug 15, 2015
 *      Author: daniel
 */

#ifndef RESOURCES_FONTS_H_
#define RESOURCES_FONTS_H_

#include <iostream>
#include <SFML/Graphics.hpp>

using namespace std;

namespace game
{

class Fonts
{
public:
	static Fonts& getInstance();
	sf::Font font;
private:
	Fonts(): loaded(false){};
	Fonts(Fonts const&)           = delete;
	void operator=(Fonts const&)  = delete;
	bool loaded;
};

} /* namespace game */

#endif /* RESOURCES_FONTS_H_ */
