/*
 * Control.h
 *
 *  Created on: Aug 15, 2015
 *      Author: daniel
 */

#ifndef ENGINE_UI_CONTROL_H_
#define ENGINE_UI_CONTROL_H_

#include <SFML/Graphics.hpp>
#include "../actions/ActionSender.h"

namespace game
{

class Control: public ActionSender
{
public:
	Control();
	virtual ~Control();

	void setEnabled(bool value);

protected:
	bool enabled;
};

} /* namespace game */

#endif /* ENGINE_UI_CONTROL_H_ */
