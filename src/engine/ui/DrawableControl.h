/*
 * DrawableControl.h
 *
 *  Created on: Aug 15, 2015
 *      Author: daniel
 */

#ifndef ENGINE_UI_DRAWABLECONTROL_H_
#define ENGINE_UI_DRAWABLECONTROL_H_

#include "Control.h"
#include <SFML/Graphics.hpp>

namespace game
{

class DrawableControl: public Control, public sf::Drawable
{
public:
	DrawableControl(DrawableControl* lpParent);
	virtual ~DrawableControl();

	virtual sf::Vector2f getAbsolutePosition() const;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	const sf::Rect<float>& getRect() const;
	void setRect(const sf::Rect<float>& rect);

	sf::Vector2f getPosition() const;
	virtual void setPosition(const sf::Vector2f& position);

	sf::Vector2f getSize() const;
	virtual void setSize(const sf::Vector2f& size);

protected:
	sf::Rect<float> rect;
	vector<DrawableControl*> lpChildren;
	DrawableControl* lpParent;
};

} /* namespace game */

#endif /* ENGINE_UI_DRAWABLECONTROL_H_ */
