/*
 * GameEngine.cpp
 *
 *  Created on: Aug 14, 2015
 *      Author: daniel
 */

#include "GameEngine.h"

namespace game
{

GameEngine::GameEngine()
{
	this->window.create(sf::VideoMode(800, 400), "Hello World!");

	this->lpLoadingScreen = new LoadingScreen(this->window);
	this->lpLoadingScreen->addActionListener(this);
	this->lpStartScreen = new StartScreen(this->window);
	this->lpStartScreen->addActionListener(this);
	this->lpCurrentScreen = this->lpLoadingScreen;
}

GameEngine::~GameEngine()
{
	delete this->lpStartScreen;
	delete this->lpLoadingScreen;
}

void GameEngine::actionPerformed(const Action& action)
{
	if(action.getSender().getId() == this->lpLoadingScreen->getId())
	{
		switch(action.getId())
		{
		case LoadingScreen::ActionID::QUIT:
			this->quit();
			break;
		case LoadingScreen::ActionID::GOTO_STARTSCREEN:
			this->changeScreen(this->lpStartScreen);
			break;
		}
	}
	else if(action.getSender().getId() == this->lpStartScreen->getId())
	{
		switch(action.getId())
		{
		case StartScreen::ActionID::GOTO_LOADINGSCREEN:
			this->changeScreen(this->lpLoadingScreen);
			break;
		}
	}
}

} /* namespace game */
