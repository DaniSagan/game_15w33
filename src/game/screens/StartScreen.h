/*
 * StartScreen.h
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#ifndef STARTSCREEN_H_
#define STARTSCREEN_H_

#include "../../engine/screens/Screen.h"
#include <iostream>

using namespace std;

namespace game
{

class StartScreen: public Screen
{
public:
	StartScreen(sf::RenderWindow& window);
	virtual ~StartScreen();

	virtual void tick();
	virtual void update();
	virtual void handleInput();
	virtual void draw();
	virtual void quit();

	virtual void actionPerformed(const Action& action);

	enum ActionID
	{
		GOTO_LOADINGSCREEN
	};
};

} /* namespace game */

#endif /* STARTSCREEN_H_ */
