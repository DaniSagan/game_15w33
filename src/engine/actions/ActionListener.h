/*
 * ActionListener.h
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#ifndef ENGINE_ACTIONS_ACTIONLISTENER_H_
#define ENGINE_ACTIONS_ACTIONLISTENER_H_

namespace game
{

class Action;

class ActionListener
{
public:
	ActionListener();
	virtual ~ActionListener();

	virtual void actionPerformed(const Action& action) = 0;
};

} /* namespace game */

#endif /* ENGINE_ACTIONS_ACTIONLISTENER_H_ */
