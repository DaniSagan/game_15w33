/*
 * GameEngine.h
 *
 *  Created on: Aug 14, 2015
 *      Author: daniel
 */

#ifndef GAME_GAMEENGINE_H_
#define GAME_GAMEENGINE_H_

#include "../engine/Engine.h"

namespace game
{

class GameEngine: public Engine
{
public:
	GameEngine();
	virtual ~GameEngine();

	void actionPerformed(const Action& action);
private:
	Screen* lpLoadingScreen;
	Screen* lpStartScreen;
};

} /* namespace game */

#endif /* GAME_GAMEENGINE_H_ */
