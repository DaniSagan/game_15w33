/*
 * Action.cpp
 *
 *  Created on: Aug 11, 2015
 *      Author: daniel
 */

#include "Action.h"
#include "ActionSender.h"

namespace game
{

Action::Action(ActionSender& sender, int id):
		sender(sender), id(id)
{
	// TODO Auto-generated constructor stub

}

Action::~Action()
{
	// TODO Auto-generated destructor stub
}

const ActionSender& Action::getSender() const
{
	return this->sender;
}

int Action::getId() const
{
	return this->id;
}

} /* namespace game */
